//
//  Location.m
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import "Location.h"

@implementation Location
@synthesize formattedAddress;
@synthesize lat;
@synthesize lng;

- (NSString *)title
{
  return [NSString stringWithFormat:@"%@", self.formattedAddress];
}

- (NSString *)subtitle
{
  return [NSString stringWithFormat:@"(%@ %@)", self.lat, self.lng];
}

- (CLLocationCoordinate2D)coordinate
{
  return CLLocationCoordinate2DMake([self.lat doubleValue], [self.lng doubleValue]);
}

@end
