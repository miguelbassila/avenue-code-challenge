//
//  LocationCommunicator.h
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationCommunicatorDelegate.h"

@interface LocationCommunicator : NSObject
@property (weak, nonatomic) id<LocationCommunicatorDelegate> delegate;

- (void)searchLocationsWithString:(NSString *)string;

@end
