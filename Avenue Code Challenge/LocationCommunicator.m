//
//  LocationCommunicator.m
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>

#import "LocationCommunicator.h"

@implementation LocationCommunicator
- (void)searchLocationsWithString:(NSString *)string
{
  NSString *newString = [self fixString:string];
  
  NSString *address = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=false", newString];
  
  NSLog(@"%@", address);
  
  AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  manager.responseSerializer = [AFJSONResponseSerializer serializer];
  
  [manager POST:address parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
          [self.delegate receivedLocationsJSON:responseObject];
        }
        failure:^(AFHTTPRequestOperation *operation, NSError *error) {
          [self.delegate fetchingLocationsFailedWithError:error];
        }
   ];
}

- (NSString *)fixString:(NSString *)string
{
  NSData *stringASCIIData = [string dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
  NSString *newString = [[NSString alloc] initWithData:stringASCIIData encoding:NSUTF8StringEncoding];
  newString = [newString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
  newString = [newString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
  
  return newString;
}

@end
