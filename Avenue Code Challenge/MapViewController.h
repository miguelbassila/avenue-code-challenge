//
//  MapViewController.h
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface MapViewController : UIViewController <MKMapViewDelegate>
@property (strong, nonatomic) NSArray *locations;

@end
