//
//  MapViewController.m
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import "MapViewController.h"
#import "Location.h"

@interface MapViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *map;

@end

@implementation MapViewController

#pragma mark Lifecycle methods

-(void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self.map addAnnotations:self.locations];
  
  if ([self.locations count] > 1) {
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in self.map.annotations){
      MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
      MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
      zoomRect = MKMapRectUnion(zoomRect, pointRect);
    }
    [self.map setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(10, 10, 10, 10) animated:YES];
    
  } else {
    id <MKAnnotation> annotation = [self.map.annotations lastObject];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(annotation.coordinate, 10000, 10000);
    [self.map setRegion:region animated:YES];
    
  }
}

-(void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  [self.map removeAnnotations:self.locations];
}

#pragma mark MapKit Delegate methods

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
  if([annotation isKindOfClass:[MKUserLocation class]]){
    return nil;
  }
  static NSString *mapPool = @"Pool";
  MKPinAnnotationView *pin = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:mapPool];
  if(!pin){
    pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:mapPool];
  }else{
    pin.annotation = annotation;
  }
  pin.pinColor = MKPinAnnotationColorRed;
  pin.canShowCallout = YES;
  return pin;
}

@end
