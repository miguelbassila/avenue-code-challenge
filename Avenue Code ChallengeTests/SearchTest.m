//
//  SearchTest.m
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/9/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>


@interface SearchTest : XCTestCase

@end

@implementation SearchTest

- (void)testSearchBarText
{
  NSString *searchBarText = @"Springfield";
  XCTAssertTrue([searchBarText isEqualToString:@"Springfield"], "searchBarText not equal");
}

- (void)testLeadingTrailingSpaces
{
  NSString *searchBarText = @" Springfield ";
  searchBarText = [searchBarText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
  XCTAssertTrue([searchBarText isEqualToString:@"Springfield"], "Still have leading and trailing spaces");
}

- (void)testAccents
{
  NSString *searchBarText = @"São Paulo";
  NSData *stringASCIIData = [searchBarText dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
  NSString *newString = [[NSString alloc] initWithData:stringASCIIData encoding:NSUTF8StringEncoding];
  XCTAssertTrue([newString isEqualToString:@"Sao Paulo"], "Still have utf-8 accents");
}

@end
