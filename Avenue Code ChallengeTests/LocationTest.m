//
//  LocationTest.m
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/9/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

@interface Location : NSObject
@property (copy, nonatomic) NSString *formattedAddress;
@property (strong, nonatomic) NSNumber *lat;
@property (strong, nonatomic) NSNumber *lng;

@end

@implementation Location
@synthesize formattedAddress, lat, lng;

@end

@interface LocationTest : XCTestCase
@property (strong, nonatomic) Location *location;

@end

@implementation LocationTest

- (void)setUp
{
  [super setUp];
  self.location = [[Location alloc] init];
}

- (void)tearDown
{
  self.location = nil;
  [super tearDown];
}

- (void)testLocationName
{
  self.location.formattedAddress = @"Springfield";
  XCTAssertTrue([[self.location formattedAddress] isEqualToString:@"Springfield"], @"String are not equal.");
}

- (void)testCoordinates
{
  self.location.lat = @(37.2089572);
  self.location.lng = @(-93.29229889999999);
  NSString *coordinates = [NSString stringWithFormat:@"(%@) - (%@)", self.location.lat, self.location.lng];
  XCTAssertNotNil(coordinates, @"Could not define lat and lng");
}



@end
