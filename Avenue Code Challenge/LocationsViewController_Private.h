//
//  LocationsViewController_Private.h
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/9/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import "LocationsViewController.h"

@interface LocationsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
