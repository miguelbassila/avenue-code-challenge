//
//  LocationManager.m
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import "LocationHandler.h"
#import "LocationCommunicator.h"
#import "LocationFactory.h"

@implementation LocationHandler
- (void)fetchLocationsWithString:(NSString *)string
{
  [self.communicator searchLocationsWithString:string];
}

#pragma mark - LocationCommunicatorDelegate methods
- (void)receivedLocationsJSON:(NSDictionary *)object
{
  NSError *error = nil;
  NSArray *locations = [LocationFactory locationsFromJSON:object error:&error];
  
  if (error) {
    [self.delegate fetchingLocationsFailedWithError:error];
  } else {
    [self.delegate didReceiveLocations:locations];
  }
}

- (void)fetchingLocationsFailedWithError:(NSError *)error
{
  [self.delegate fetchingLocationsFailedWithError:error];
}

@end
