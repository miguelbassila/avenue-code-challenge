//
//  LocationsViewController.h
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/7/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationsViewController : UIViewController <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>

@end
