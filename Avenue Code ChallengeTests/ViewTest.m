//
//  ViewTest.m
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/9/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "LocationsViewController_Private.h"

@interface ViewTest : XCTestCase
@property (nonatomic, strong) LocationsViewController *vc;
@end

@implementation ViewTest

- (void)setUp
{
  [super setUp];
  UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
  self.vc = [storyboard instantiateViewControllerWithIdentifier:@"locationsTableView"];
  [self.vc performSelectorOnMainThread:@selector(loadView) withObject:nil waitUntilDone:YES];
}

- (void)tearDown {
  self.vc = nil;
  [super tearDown];
}

-(void)testThatViewLoads
{
  XCTAssertNotNil(self.vc.view, @"View not initiated properly");
}

- (void)testParentViewHasTableViewSubview
{
  NSArray *subviews = self.vc.view.subviews;
  XCTAssertTrue([subviews containsObject:self.vc.tableView], @"View does not have a table subview");
}

-(void)testThatTableViewLoads
{
  XCTAssertNotNil(self.vc.tableView, @"TableView not initiated");
}

#pragma mark - UITableView tests
- (void)testThatViewConformsToUITableViewDataSource
{
  XCTAssertTrue([self.vc conformsToProtocol:@protocol(UITableViewDataSource) ], @"View does not conform to UITableView datasource protocol");
}

- (void)testThatTableViewHasDataSource
{
  XCTAssertNotNil(self.vc.tableView.dataSource, @"Table datasource cannot be nil");
}

- (void)testThatViewConformsToUITableViewDelegate
{
  XCTAssertTrue([self.vc conformsToProtocol:@protocol(UITableViewDelegate) ], @"View does not conform to UITableView delegate protocol");
}

- (void)testTableViewIsConnectedToDelegate
{
  XCTAssertNotNil(self.vc.tableView.delegate, @"Table delegate cannot be nil");
}

@end
