//
//  CommunicationTest.m
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/9/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import <AFNetworking/AFHTTPRequestOperation.h>
#import <AFNetworking/AFHTTPRequestOperationManager.h>

@interface CommunicationTest : XCTestCase
@property (strong, nonatomic) AFHTTPRequestOperationManager *manager;
@end

@implementation CommunicationTest

- (void)setUp
{
  [super setUp];
  self.manager = [AFHTTPRequestOperationManager manager];
  self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
  self.manager.responseSerializer = [AFJSONResponseSerializer serializer];
}

- (void)disabled_testResponseObject
{
  NSString *address = @"http://maps.googleapis.com/maps/api/geocode/json?address=Springfield&sensor=false";
  [self.manager POST:address parameters:nil  success:^(AFHTTPRequestOperation *operation, id responseObject){
    XCTAssert(responseObject, @"Null response");
  }failure:^(AFHTTPRequestOperation *operation, NSError *error){
    NSLog(@"%@", [error localizedDescription]);
  }];
}

- (void)testResponseObjectIsDictionaryWithResultsKey
{
  NSString *address = @"http://maps.googleapis.com/maps/api/geocode/json?address=Springfield&sensor=false";
  [self.manager POST:address parameters:nil  success:^(AFHTTPRequestOperation *operation, id responseObject){
    NSDictionary *dictionary = [NSDictionary dictionaryWithDictionary:responseObject];
    NSArray *results = [dictionary objectForKey:@"results"];
    XCTAssertTrue([results count], @"No results key");
  }failure:^(AFHTTPRequestOperation *operation, NSError *error){
    NSLog(@"%@", [error localizedDescription]);
  }];
}

@end
