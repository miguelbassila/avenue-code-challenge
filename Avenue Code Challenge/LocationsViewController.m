//
//  LocationsViewController.m
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/7/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import "LocationsViewController.h"
#import "LocationHandler.h"
#import "LocationCommunicator.h"
#import "Location.h"
#import "MapViewController.h"

@interface LocationsViewController () <LocationHandlerDelegate>
@property (strong, nonatomic) LocationHandler *handler;
@property (strong, nonatomic) NSArray *locations;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UILabel *noResults;

@end

@implementation LocationsViewController

#pragma mark Lifecycle methods

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.handler = [[LocationHandler alloc] init];
  self.handler.communicator = [[LocationCommunicator alloc] init];
  self.handler.communicator.delegate = _handler;
  self.handler.delegate = self;
  
  self.noResults.hidden = YES;
  
  [self addGestureHideKeyboard];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
  [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
  [self.view endEditing:YES];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if ([segue.identifier isEqualToString:@"showMap"]) {
    
    NSArray *sourceArray = self.locations;
    NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell *)sender];
    
    if ([self.tableView numberOfSections] == 2 && indexPath.section != 0){
      Location *location = sourceArray[indexPath.row];
      sourceArray = @[location];
    }
    
    MapViewController *destinationController = segue.destinationViewController;
    destinationController.locations = sourceArray;
  }
}

#pragma mark - UISearch Delegates and related methods

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
  [self.handler fetchLocationsWithString:searchBar.text];
  [searchBar resignFirstResponder];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
  [searchBar resignFirstResponder];
}

#pragma mark - UITableView Datasource and delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  if ([self numberOfSectionsInTableView:tableView] == 2 && section == 0) {
    return 1;
  } else {
    return [self.locations count];
  }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  if ([self.locations count] > 1) {
    return 2;
  }else {
    return 1;
  }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LocationCell"];
  
  if ([self.locations count] > 1 && indexPath.section == 0) {
    cell.textLabel.text = @"Display All On Map";
    return cell;
  }
  
  Location *location = [self.locations objectAtIndex:indexPath.row];
  cell.textLabel.text = location.formattedAddress;
  
  return cell;
}

#pragma mark - LocationHandler Delegate methods

- (void)didReceiveLocations:(NSArray *)locations
{
  if ([locations count]) {
    self.locations = locations;
    [self.tableView reloadData];
    [self resultsTableView:NO noResultsLabel:YES];
  } else {
    [self resultsTableView:YES noResultsLabel:NO];
  }
}

- (void)fetchingLocationsFailedWithError:(NSError *)error
{
  NSLog(@"Error: %@", [error localizedDescription]);
}

#pragma mark - Custom methods
- (void)addGestureHideKeyboard{
  UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                      action:@selector(hideKeyboard)];
  gestureRecognizer.cancelsTouchesInView = NO;
  [self.tableView addGestureRecognizer:gestureRecognizer];
}

- (void) hideKeyboard {
  [self.searchBar resignFirstResponder];
}

- (void)resultsTableView:(BOOL)optionTable noResultsLabel:(BOOL)optionLabel
{
  self.tableView.hidden = optionTable;
  self.noResults.hidden = optionLabel;
}

@end
