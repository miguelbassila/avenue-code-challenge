//
//  LocationFactory.m
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import "LocationFactory.h"
#import "Location.h"

@implementation LocationFactory

+ (NSArray *)locationsFromJSON:(NSDictionary *)object error:(NSError **)error
{
  NSMutableArray *locations = [[NSMutableArray alloc] init];
  
  NSArray *results = [object valueForKey:@"results"];
  
  if ([results count]) {
    for (NSDictionary *dic in results) {
      Location *location = [[Location alloc] init];
      location.formattedAddress = [dic objectForKey:@"formatted_address"];
      location.lat = [[[dic objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lat"];
      location.lng = [[[dic objectForKey:@"geometry"] objectForKey:@"location"] objectForKey:@"lng"];
      
      [locations addObject:location];
    }
  }
  
  return locations;
}

@end
