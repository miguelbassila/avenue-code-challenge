//
//  LocationCommunicatorDelegate.h
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LocationCommunicatorDelegate <NSObject>
- (void)receivedLocationsJSON:(NSData *)object;
- (void)fetchingLocationsFailedWithError:(NSError *)error;
@end
