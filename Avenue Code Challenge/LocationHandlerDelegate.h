//
//  LocationHandlerDelegate.h
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LocationHandlerDelegate <NSObject>
- (void)didReceiveLocations:(NSArray *)locations;
- (void)fetchingLocationsFailedWithError:(NSError *)error;
@end
