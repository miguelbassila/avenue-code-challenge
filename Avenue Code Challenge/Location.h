//
//  Location.h
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Location : NSObject <MKAnnotation>
@property (copy, nonatomic) NSString *formattedAddress;
@property (strong, nonatomic) NSNumber *lat;
@property (strong, nonatomic) NSNumber *lng;

@end
