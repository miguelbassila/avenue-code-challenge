//
//  LocationHandler.h
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LocationHandlerDelegate.h"
#import "LocationCommunicatorDelegate.h"

@class LocationCommunicator;

@interface LocationHandler : NSObject <LocationCommunicatorDelegate>
@property (strong, nonatomic) LocationCommunicator *communicator;
@property (weak, nonatomic) id<LocationHandlerDelegate> delegate;

- (void)fetchLocationsWithString:(NSString *)string;

@end
