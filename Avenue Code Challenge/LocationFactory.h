//
//  LocationFactory.h
//  Avenue Code Challenge
//
//  Created by Miguel Bassila on 9/8/14.
//  Copyright (c) 2014 Miguel Bassila. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationFactory : NSObject
+ (NSArray *)locationsFromJSON:(NSDictionary *)object error:(NSError **)error;

@end
